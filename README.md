
# Crearem el nostre arxiu de docker en base al nostre directori on estem que sera on esta tots els
# procesos, en aquets case net22:base que esta al fer el git clone més el link del projecte del web-22

docker build -t bryan501/net22:base .

# Desprès fem el docker run per que s'executi la imatge de docker 

docker run --rm -h net.edt.org -d bryan501/net22:base

# Altres comandes a tenir en compte:

git add . ; git commit -m "m01 exemple" ; git push

docker login 
